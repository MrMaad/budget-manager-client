import { Component } from '@angular/core';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
      trigger("sidebarAnimation", [
          state("shown", style({
            transform: 'translateX(0)',
          })),
          state("hidden", style({
            transform: 'translateX(-250px)',
          })),
          transition('hidden <=> shown', animate('500ms ease-in-out'))
      ]),
      trigger("contentAnimation", [
        state("shown", style({
          transform: 'translateX(0)',
        })),
        state("hidden", style({
          transform: 'translateX(2000px)',
        })),
        transition('hidden <=> shown', animate('1500ms ease-in-out'))
    ]),
      trigger("loginboxAnimation", [
        state("shown", style({
            opacity: 1,
        })),
        state("hidden", style({
            opacity: 0,
            transform: 'translateX(-100%)'
        })),
        transition("shown => hidden", animate("500ms ease-in-out", keyframes([
            style({opacity: 1,  offset: 0}),
            style({opacity: 0,  offset: 0.5}),
            style({opacity: 0, display: "none", offset: 1.0})
          ])))
    ])
  ]
})
export class AppComponent {
    title = 'app';
    sidebarState = "shown";
    contentState = "shown";
    loginboxState = "hidden";

    
    hideLoginbox() {
        this.loginboxState = this.loginboxState == "shown" ? "hidden" : "shown";
        this.toggleSidebar();
        this.toggleContent();
    }

    toggleSidebar() {
        this.sidebarState = (this.sidebarState =="shown" ? "hidden" : "shown");
    }

    toggleContent() {
        this.contentState = (this.contentState == "shown" ? "hidden" : "shown");
    }
}
