import { Component, OnInit } from '@angular/core';
import { HttpService } from './../services/http.service';
import { Income } from '../interfaces/income';


@Component({
	selector: 'incomes',
	templateUrl: './incomes.component.html',
	styleUrls: ['./incomes.component.css']
})


export class IncomeComponent implements OnInit {

	incomes: Income;
	data_fetching = true;
	constructor(private service: HttpService) {
	}

	ngOnInit() {
		this.service.getIncomes()
			.subscribe(data => {
				this.incomes = {
					status: data['status'],
					data: data['data'],
			}
			
			this.data_fetching = false;
		});
	}
}
