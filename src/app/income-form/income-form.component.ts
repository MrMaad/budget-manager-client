import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';
import { Income } from '../interfaces/income';
import { FormGroup, FormControl, Validators, Form } from '@angular/forms';

@Component({
  selector: 'app-income-form',
  templateUrl: './income-form.component.html',
  styleUrls: ['./income-form.component.css']
})
export class IncomeFormComponent implements OnInit {

  form = new FormGroup({});
  incomes: Income;
  data_fetching = true;
  form_keys;
  constructor(private service: HttpService) { }

  ngOnInit() {
    this.service.getIncomesForm()
      .subscribe(data => {
        this.incomes = {
          status: data['status'],
          data: data['data'],
        };
        this.createForm();
        this.data_fetching = false;
      });
  }

  postIncome() {
    
    this.service.postIncome(this.form.value);
  }

  createForm() {
   this.form = new FormGroup({})
   this.incomes.data.forEach(element => {
     let field = new FormControl('', Validators.required);
     this.form.addControl(element.name, field);
   });
   console.log(this.form.controls);
   this.form_keys = Object.keys(this.form.controls);
  }

}
