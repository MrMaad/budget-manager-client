import { Component, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EventEmitter } from '@angular/core';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
    myform: FormGroup;
    langs: string[] = [
        "polski",
        "English",
    ];

    @Output() loggedIn = new EventEmitter();
  
    ngOnInit() {
        this.myform = new FormGroup({
            login: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required),
        });
    }

    onSubmit() {
        if (this.myform.valid) {
            this.loggedIn.emit('logged');
            console.log(this.myform.value);
        } else {
            console.log("Invalid!");
        }
    }
}