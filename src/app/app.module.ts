import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { RootComponent } from './root/root.component';
import { HttpService } from './services/http.service';
import { ExpensesComponent } from './expenses/expenses.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { IncomeComponent } from './incomes/incomes.component';

import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { IncomeFormComponent } from './income-form/income-form.component';


const routes: Routes = [
	{ path: 'root', component: RootComponent, pathMatch: 'full' },
	{ path: 'expenses', component: ExpensesComponent, pathMatch: 'full' },
	{ path: 'login', component: LoginComponent },
	{ path: 'incomes', component: IncomeComponent },
	{ path: 'income/new', component: IncomeFormComponent },
	{ path: '', component: HomeComponent },
];

@NgModule({
	declarations: [
		AppComponent,
		RootComponent,
		ExpensesComponent,
		HomeComponent,
		LoginComponent,
		IncomeComponent,
		IncomeFormComponent,
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		RouterModule.forRoot(routes, {useHash: false}),
        ReactiveFormsModule,
        BrowserAnimationsModule,
	],
	providers: [
		HttpService,
	],
	bootstrap: [AppComponent]
})



export class AppModule { }
