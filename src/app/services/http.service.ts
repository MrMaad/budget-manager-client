import { Income } from './../interfaces/income'
import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'

@Injectable()
export class HttpService {

    constructor(private http: HttpClient) { }

    getIncomes() {
        return this.http.get<Income[]>('http://localhost:8000/api/incomes');
    }

    getIncomesForm() {
        return this.http.get('http://localhost:8000/api/incomes/new');
    }

    postIncome(data) {
        this.http.post('http://localhost:8000/api/incomes/new', data)
        .subscribe(response => console.log(response));
    }
}